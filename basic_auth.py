from jira.client import JIRA
import logging

# Defines a function for connecting to Jira
def basic_auth(jira_server, jira_user, jira_password):
    '''
    Connect to JIRA. Return None on error
    '''

    try:
        jira_options = {'server': jira_server}
        jira = JIRA(options=jira_options, basic_auth=(jira_user, jira_password))
        # ^--- Note the tuple
        return jira
    except Exception as e:
        return None
