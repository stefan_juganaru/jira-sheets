from jira.client import JIRA
import logging

def get_issue(jira, project_name, jql_string, start_at, max_results, fields, json_result):

    if not jql_string:
        final_jql = "project=%s" % (project_name)
    elif "order" in jql_string:
        final_jql = "project=%s %s" % (project_name, jql_string)
    elif "ORDER" in jql_string:
        final_jql = "project=%s %s" % (project_name, jql_string)
    else:
        final_jql = "project=%s and %s" % (project_name, jql_string)

    block_size = 50
    block_num = 0
    while True:
        start_idx = block_num*block_size
        issues = jira.search_issues(final_jql, start_idx, block_size)
        if len(issues) == 0:
            break
        block_num += 1

        for issue in issues:
            print('%s: %s' % (issue.key, issue.fields.summary))

# def _search_issue_with_starting_index(jql_string, start_at):

#     return jira.search_issues(jql_string, startAt=start_at)

# def _search_issue_default(jql_string):

#     return jira.search_issues(jql_string)

# def _search_issue_with_max_results(jql_string, max_results):

#     return jira.search_issues(jql_string, maxResults=max_results)
