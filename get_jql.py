def get_jql(project_name, jql_string):

    final_jql = "d"

    if not jql_string:
        final_jql = "project=%s" % (project_name)
    elif "order" in jql_string:
        final_jql = "project=%s %s" % (project_name, jql_string)
    elif "ORDER" in jql_string:
        final_jql = "project=%s %s" % (project_name, jql_string)
    else:
        final_jql = "project=%s and %s" % (project_name, jql_string)

    return final_jql
